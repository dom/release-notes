#!/usr/bin/env python3

import glob
import os.path
import re
import sys

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("call with original language directory and one or more", file=sys.stderr)
        print("translation directories, e.g. %s en ca cs pt_BR" % sys.argv[0], file=sys.stderr)
        sys.exit(-1)
    origlang = sys.argv[1]
    targetlangs = sys.argv[2:]
    filenames = list(map(os.path.basename, glob.glob("%s/*.dbk" % origlang)))
    sizes = {}
    for fn in filenames:
        sizes[fn] = len(open("%s/%s" % (origlang, fn), "r").readlines())
    total = sum(sizes.values())
    revisions = {}
    for fn in filenames:
        for attr in os.popen("git log --format=%%H -n 1 %s/%s" % (origlang, fn)).readlines():
            revisions[fn] = attr
    # looking for marker: "^<!-- English version: 1234 -->$"
    rev_re = re.compile("^\s*<!--\s+English version:\s+([0-9a-z]+)\s+-->\s*$")
    for lng in targetlangs:
        stats = 0
        for fn in filenames:
            for line in open("%s/%s" % (lng, fn), "r").readlines(10):
                match = rev_re.match(line)
                if match:
                    revision = int(rev_re.match(line).group(1))
                    if revision == revisions[fn]:
                        # Current! Count 100%.
                        stats += sizes[fn]
                    else:
                        # Git revision not current. Count 50%.
                        stats += sizes[fn]/2.
        value = float(stats)/(total/100.)
        print("%s: ~%d%% (~%.2f%%)." % (lng, int(round(value)), value))
